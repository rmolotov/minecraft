﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public RectTransform[] craftSlots;
	public Transform backpack;
	public int[] blender;
	bool crafted;

	public void Craft () {
		crafted = false;
		blender = new int[5];
		foreach (Transform slot in craftSlots) {
			if (slot.childCount > 0) {
				int id = int.Parse(slot.GetChild (0).GetComponent<Image> ().sprite.name);
				blender[id] = blender[id] + 1;
			}
		}
		int blockID = System.Array.IndexOf (blender, 4);
		if (blockID >= 0) {
			backpack.GetComponent<BackpackScript> ().ChangeMatCount (blockID, -4);
			backpack.GetComponent<BackpackScript> ().ChangeBlockCount (blockID , 1);
			crafted = true;
		}

		if (crafted == true) {
			//clean slots
			foreach (Transform slot in craftSlots) {
				foreach (Transform child in slot)
					Destroy (child.gameObject);
			}
		}
	}
}
