﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

	public Transform startParent = null;

	public void OnBeginDrag (PointerEventData eventData) {
		startParent = this.transform.parent;
		this.transform.SetParent (this.transform.GetComponentInParent<UIManager>().transform);
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	public void OnDrag (PointerEventData eventData) {
		this.transform.position = eventData.position;
	}

	public void OnEndDrag (PointerEventData eventData) {
		this.transform.SetParent (startParent);
		this.transform.localPosition = Vector3.zero;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
	}
}
