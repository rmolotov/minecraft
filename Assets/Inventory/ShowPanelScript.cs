﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class ShowPanelScript : MonoBehaviour {

	bool visible;
	public GameObject panel;
	public Transform slotPrefab;
	public Transform[] craftSlots;

	RectTransform rt;
	BackpackScript bp;

	void Start ()
	{
		panel.SetActive (false);
		visible = false;
		rt = panel.GetComponentInChildren<ScrollRect> ().content;
		bp = GetComponentInChildren<BackpackScript> ();
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.I)) {
			if (visible) {
				panel.SetActive (false);
				GetComponent<FirstPersonController> ().enabled = true;
				Time.timeScale = 1;
				UnityEngine.Cursor.visible = false;
				visible = false;

				foreach (Transform child in rt)
					Destroy (child.gameObject);
				
				foreach (Transform slot in craftSlots)
					foreach (Transform child in slot)
						Destroy (child.gameObject);
				
			} else {
				panel.SetActive (true);
				GetComponent<FirstPersonController> ().enabled = false;
				Time.timeScale = 0;
				Cursor.lockState = CursorLockMode.None;
				UnityEngine.Cursor.visible = true;
				visible = true;

				for (int i = 0; i < bp.materials.Length; i++) {
					for (int j = 0; j < bp.materials [i]; j++) {
						Transform slot = Instantiate (slotPrefab);
						slot.SetParent (rt.transform);
						slot.GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> (i.ToString ());
					}
				}

				rt.sizeDelta = new Vector2 (320, 64 * (rt.transform.childCount / 5 + 1));
				//resize here
			}
		}
	}
}
