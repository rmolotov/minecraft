﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler {

	public void OnDrop(PointerEventData eventData) {
		DragHandler item = eventData.pointerDrag.GetComponent<DragHandler> ();
		if (item != null && this.transform.childCount == 0) {
			item.startParent = this.transform;
		}
	}

}
