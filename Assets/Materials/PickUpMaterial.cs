﻿using UnityEngine;
using System.Collections;

public class PickUpMaterial : MonoBehaviour {

	public int MaterialId;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			other.GetComponentInChildren<BackpackScript> ().ChangeMatCount(MaterialId, 1);
			Destroy (this.gameObject);
		}	
	}
}
