﻿using UnityEngine;
using System.Collections;

public class BlockScript : MonoBehaviour {

	public Transform[] cracksPrefs;

	[Range(0, 100)]
	public int health = 100;

	public void GetDamage (int damage) {
		health -= damage;
		if (health <= 0) {
			int id = int.Parse(GetComponent<Renderer>().material.mainTexture.name);
			Instantiate (cracksPrefs[id], transform.position, transform.rotation);
			Destroy (gameObject);
		}
	}
}
