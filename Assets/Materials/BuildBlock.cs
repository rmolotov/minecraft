﻿using UnityEngine;
using System.Collections;

public class BuildBlock : MonoBehaviour {

	BackpackScript bp;
	Camera cam;
	int BLOCK_SIZE = 1;

	public Transform[] blockPrefabs;
	public Transform landscape;
	public Transform marker;

	void Start () {
		bp = GetComponentInChildren<BackpackScript> ();
		cam = GetComponentInChildren<Camera> ();
	}

	void Update () {
		Ray ray = new Ray (cam.transform.position, cam.transform.forward);
		RaycastHit hit = new RaycastHit ();
		Physics.Raycast (ray, out hit, 5);
		Collider target = hit.collider;

		if (target != null && target.gameObject.tag.Contains ("Block") && bp.blocks [bp.selectedInd] >= 0) {
			Vector3 pos = target.transform.position + hit.normal * BLOCK_SIZE;
			marker.position = pos;
			marker.rotation = target.transform.rotation;
			marker.GetComponent<Renderer> ().enabled = true;
			if (Input.GetMouseButtonDown(1)) {
				Transform block = (Transform)Instantiate (blockPrefabs[bp.selectedInd], marker.position, marker.rotation);
				block.SetParent (landscape);

			}
		} else {
			marker.GetComponent<Renderer> ().enabled = false;
		}
	}
}
