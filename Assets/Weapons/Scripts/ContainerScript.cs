﻿using UnityEngine;
using System.Collections;

public class ContainerScript : MonoBehaviour {

	Animator _animator;
	Camera cam;
	// Use this for initialization
	void Start () {
		_animator = GetComponent<Animator> ();
		cam = transform.parent.GetComponentInChildren<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			_animator.PlayInFixedTime ("ContainerAnimation");
		
			Ray ray = new Ray(cam.transform.position, cam.transform.forward);
			RaycastHit hit = new RaycastHit();
			Physics.Raycast(ray, out hit, 2);
			Collider target = hit.collider;

			if (target != null && this.transform.childCount >= 0)
			{
				if (target.gameObject.GetComponent<BlockScript>() != null)
					target.gameObject.GetComponent<BlockScript>().GetDamage(10);
				if (target.gameObject.GetComponent<Enemy>() != null)
					target.gameObject.GetComponent<Enemy>().GetDamage(10);
			}
		}
	}
}
