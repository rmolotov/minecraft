﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickUpNDropWeapon : MonoBehaviour {

	public Image crosshair;
	public Sprite idleCrosshair;
	public Sprite overCrosshair;
	public Transform container;
	public Sprite icon;
	public Image HUDSlotImg;

	void OnMouseEnter() {
		crosshair.sprite = overCrosshair;

	}
	void OnMouseExit() {
		crosshair.sprite = idleCrosshair;
	}

	void OnMouseOver() {
		if (Input.GetMouseButtonDown(1)) {
			if (container.childCount > 0 && container.GetChild (0).GetComponent<Rigidbody> () != null) {
				container.GetChild (0).GetComponent<Rigidbody> ().isKinematic = false;
				container.DetachChildren ();
			}

			transform.SetParent (container);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.Euler (Vector3.zero);
			GetComponent<Rigidbody> ().isKinematic = true;
			HUDSlotImg.sprite = icon;
		}
	}
}
