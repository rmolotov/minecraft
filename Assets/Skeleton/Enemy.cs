﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    // Здоровье врага
    [Range(0, 100)]
    public int health = 100;

    // Угол и дистанция обзора врага (угол - половина, если здесь 30, то сектор обзора развернут на 60)
    [Header("View")]
    public float DistanceOfView = 20;
    [Range(0, 180)]
    public float AngleOfView = 30;

    // Закрытые, private переменные цели, игрока, аниматора и зоны поражения
    Transform target;
    Transform player;
    Animator anim;
    // Список (= List) игровых объектов (= gameObject), которым будет наноситься урон
    List<GameObject> DamageArea;

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BlockScript>() != null || other.CompareTag("Player"))
        {
            if (!DamageArea.Contains(other.gameObject))
                DamageArea.Add(other.gameObject);
        }
        else if (other.CompareTag("Block"))
        {
            // "Прыжок", чтобы смог ступеньки "перешагивать"
            GetComponent<Rigidbody>().AddRelativeForce(250 * new Vector3(0, 1, 0.2f));
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<BlockScript>() != null || other.transform == player)
        {
            DamageArea.Remove(other.gameObject);
        }
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = GameObject.FindGameObjectWithTag("Target").transform;
        DamageArea = new List<GameObject>();
    }

    void FixedUpdate()
    {
        // Если в данный момент получает урон или умер и не воспроизвели анимацию урона до конца - AI дальше не думает: return
        if (anim.GetInteger("State") >= 3 && anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            return;

        if (DamageArea.Count > 0)
        {
            // Если есть что крушить, Халк крушить, AI дальше не думает: return
            anim.SetInteger("State", 2);
            return;
        }
        else
        {
            // Иначе идем к цели
            anim.SetInteger("State", 1);
        }

        Vector3 direction = target.position - this.transform.position;
        // рассчет угла для поворота к игроку:
        //float angle = Vector3.Angle(new Vector3(direction.x, 0, direction.z), new Vector3(this.transform.forward.x, 0, this.transform.forward.z));
        direction.y = 0;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
        if (direction.magnitude > 2)
        {
            // Хотьба
            this.transform.Translate(0, 0, 0.02f);
            anim.SetInteger("State", 1);
        }
        else
        {
            // Если дошли - стоим ждем. Дописать сюда Game Over событие чере вызов нового метода
            anim.SetInteger("State", 0);
        }
    }

    public void GetDamage(int damage)
    {
        health -= damage;
        anim.SetInteger("State", 3);
        if (health <= 0)
        {
            // Умирая переходим в последнее состояние 4 с завершающей анимацией, отключаем ЭТОТ скрипт, удаляем ВСЮ физическую модель врага 
            anim.SetInteger("State", 4);
            this.enabled = false;
            Destroy(gameObject.GetComponent<Rigidbody>());
            foreach (Collider col in GetComponents<Collider>())
                Destroy(col);
        }
    }

    public void Damage()
    {
        // Вызывается при событии анимации Attack: см. Events в Inspector у Skeleton@Attack.fbx

        // Удаляем из списка на снос то, что уже СОВСЕМ сломали и уничтожили 
        DamageArea.RemoveAll(s => s == null);
            foreach (GameObject item in DamageArea)
            {
            if (item.transform == player)
            {
                // скрипт Health.cs для игрока дописать самостоятельно, на основе BlockScript.cs
                // player.GetComponent<Health>().GetDamage(5);
            }
            else
            {
                item.GetComponent<BlockScript>().GetDamage(5);
            }
        }
    }
}