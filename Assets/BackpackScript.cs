﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackpackScript : MonoBehaviour {

	//wood, stone, metal, glass, sponge
	public int[] materials;
	public int[] blocks;

	public RectTransform[] matUI;
	public RectTransform[] blocksUI;

	public int selectedInd = 0;

	void Awake () {
		materials = new int[5];
		blocks = new int[5];
		blocksUI [selectedInd].GetComponent<Image> ().color = new Color (0f, 1f, 0f, 0.392f);
	}

	public void ChangeMatCount(int ID, int count) {
		materials [ID] += count;
		matUI [ID].GetComponentInChildren<Text> ().text = materials[ID].ToString();
	}

	public void ChangeBlockCount(int ID, int count) {
		blocks [ID] += count;
		blocksUI [ID].GetComponentInChildren<Text> ().text = blocks[ID].ToString();
	}

	void Update () {
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			blocksUI [selectedInd].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 0.392f);

			selectedInd += 1 * (System.Math.Sign(Input.GetAxis ("Mouse ScrollWheel")));
			selectedInd = (5 + selectedInd) % 5;

			blocksUI [selectedInd].GetComponent<Image> ().color = new Color (0f, 1f, 0f, 0.392f);
		}
	}
}
