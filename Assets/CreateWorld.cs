﻿using UnityEngine;
using System.Collections;
using System.IO;

public class CreateWorld : MonoBehaviour {
	
	public GameObject[] blocksPrefabs;
	public int size;

	void Start () {
		if (File.Exists (Application.dataPath + "/world.txt")) {
			Load (Application.dataPath + "/world.txt");
		} else {
			Create ();
		}
	}

	void OnDestroy() {
		Save (Application.dataPath + "/world.txt");
	}

	void Create() {
		int seed = Random.Range (0, 16);
		for (int x = 0; x < size; x++)
			for (int z = 0; z < size; z++) {
				// Генерация холмистого ландшафта. Гугл: Шум Перлина
				//int y = (int)(8 * Mathf.PerlinNoise(seed + x/32.0f, seed + z/32.0f));
				// Генерация ровной поверхности на уровне 0.
				int y = 0;
				
				// Если ошибка - то здесь и на 42, слева и справа от =, вместо GameObject - Transform и block.SetParent(...
				GameObject block = (GameObject)Instantiate (blocksPrefabs[5], new Vector3 (x, y, z), Quaternion.identity);
				block.transform.SetParent (this.transform);
			}
	}

	void Load (string path) {
		string[] blocks = File.ReadAllLines (path);
		for (int i = 0; i < blocks.Length; i++) {
			string[] item = blocks [i].Split (' ');
			Vector3 pos = new Vector3 (float.Parse (item [0]), float.Parse (item [1]), float.Parse (item [2]));
			GameObject block = (GameObject)Instantiate (blocksPrefabs [int.Parse(item [3])], pos, Quaternion.identity);
			block.transform.SetParent (this.transform);
		}
	}

	void Save(string path) {
		string[] blocks = new string[transform.childCount];
		for (int i = 0; i < blocks.Length; i++) {
			string block = "";
			block += transform.GetChild(i).position.x + " ";
			block += transform.GetChild(i).position.y + " ";
			block += transform.GetChild(i).position.z + " ";
			block += transform.GetChild(i).GetComponent<Renderer>().material.name[0];
			blocks [i] = block;
		}
		File.WriteAllLines (path, blocks);
	}
}
